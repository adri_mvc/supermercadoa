package supermercado.modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author PC
 */
public class DAOProducto {
    public Producto insertar(String codProducto, String nomProducto){
       String q = "INSERT INTO producto VALUES ('"
               + codProducto+"','"
               + nomProducto+"')";
       
       //Si la inserción es correcta se retorna el objeto Producto
       if(new ConexionDB().actualizar(q)>0){
           return new Producto(codProducto,nomProducto);
       }
       return null;
    }
    
    public List obtenerRegistros(){
       String q = "SELECT * FROM producto ";
       List<Map> registros = null;
       registros = new ConexionDB().consultar(q);
       //Se contruye una lista con los productos recuperados de la DB
       List<Producto> productos = new ArrayList();
       for(Map registro: registros){
           Producto producto=new Producto((String)registro.get("codProducto"),
                   (String)registro.get("nomProducto"));
           productos.add(producto);
       }
       return productos;  
    }
    
    public int actualizar(String codProducto, String nomProducto){
       return 0; 
    }
    
    public Cliente obtenerRegistro(String codProducto){
       return null;
    }
    
    public void eliminar(String cedula){
        
    }
}
