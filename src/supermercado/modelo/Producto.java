package supermercado.modelo;

public class Producto {

    private String codProducto;
    private String nomProducto;

    public Producto() {
    }
    
    //Constructor, asigna codigo y nombre del producto
    public Producto(String codProducto, String nomProducto) {
        this.codProducto = codProducto;
        this.nomProducto = nomProducto;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public String getNomProducto() {
        return nomProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public void setNomProducto(String nomProducto) {
        this.nomProducto = nomProducto;
    }
 
}
