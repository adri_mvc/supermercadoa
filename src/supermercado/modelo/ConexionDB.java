package supermercado.modelo;

import java.sql.*;
import java.util.*;
/**
 *
 * @author PC
 */
public class ConexionDB {
    private final String DRIVER="com.mysql.cj.jdbc.Driver";
    private final String URL="jdbc:mysql://localhost:3306/pruebaplataformas";
    private final String USER="root";
    private final String PASSWORD="Mysql.1982";
    
    private Connection conexion;
    
    public ConexionDB (){
        
        try{
            Class.forName(DRIVER);
            //crea la conexión a la base de datos
            conexion = DriverManager.getConnection(URL, USER, PASSWORD);
            
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }catch(SQLException e){
            e.printStackTrace();
        }
        
    }
    
    
    
    public int actualizar(String sql){
        
        try{
            Statement st = conexion.createStatement();
            return st.executeUpdate(sql);
        }catch(SQLException e){
            e.printStackTrace();
        }
        return 0;
    }
    
    public List consultar(String sql){
        ResultSet rs = null;
        List resultado = new ArrayList();
        try{
            Statement st = conexion.createStatement();
            rs =  st.executeQuery(sql);
            resultado = asignarDatos(rs);
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public List asignarDatos(ResultSet rs){
        
        List resultado = new ArrayList();
        
        try{
           int cantColumnas = rs.getMetaData().getColumnCount(); 
           while(rs.next()){
               Map<String,Object> fila = new HashMap();
               for(int i=1;i<=cantColumnas;i++){
                   String nombreCampo=rs.getMetaData().getColumnName(i);
                   Object valor=rs.getObject(nombreCampo);
                   fila.put(nombreCampo, valor);
               }
               resultado.add(fila);
           }
        }catch(SQLException e){
            e.printStackTrace();
        }
        return resultado;
    }
    
    public boolean procedimiento(String nombreProcedim){
        
        try{
            CallableStatement st = conexion.prepareCall("{ call "+nombreProcedim+" }");
            return st.execute();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return false;
    }
 
    public void cerrarConexion(){
        
        try{
            conexion.close();
        }catch(SQLException e){
            e.printStackTrace();
        }

    }
    
}
