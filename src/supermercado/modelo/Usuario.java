package supermercado.modelo;

/**
 *
 * @author PC
 */
public class Usuario {
    
    private String iduser;
    private String username;
    private String password;
    private String privilegio;
    private String activo;

    public Usuario() {
    }

    public Usuario(String iduser, String username, String password, String privilegio, String activo) {
        this.iduser = iduser;
        this.username = username;
        this.password = password;
        this.privilegio = privilegio;
        this.activo = activo;
    }

    public String getIduser() {
        return iduser;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getPrivilegio() {
        return privilegio;
    }

    public String getActivo() {
        return activo;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPrivilegio(String privilegio) {
        this.privilegio = privilegio;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    
}
